# Bem vindo ao teste de habilidades InoBram.

Este teste tem como objetivo avaliar suas habilidades no _framework_ **Qt**. Utilizando as linguagens **QML** e **JavaScript** você deve concluir o desafio para que possamos saber qual seu nível de entendimento. 

## Avisos antes de começar

Você deve fazer um fork desse repositório e deixar o projeto em modo publico para que possamos acompanhar seu histórico de commits. 


# O Projeto

O projeto que você irá desenvolver já possui uma estrutura definida e você deverá seguir um modelo descrito nesse arquivo.


## Conteúdo inicial

Você irá fazer o clone de um projeto já existente que possui apenas uma tela inicial com um botão como é visto na imagem abaixo:

| ![img1](./images/firstPage.png) |
|:--:|
| **Página inicial** |

## Desafio

Seu desafio é criar uma segunda tela para esse projeto, essa tela deve buscar piadas em uma **API** e mostrar em um visualizador de listas como é possível verificar nas imagens abaixo.

| ![img2](./images/secondPage.png) |
|:--:|
| **Segunda página sem piadas** |


| ![img3](./images/secondPageWithData.png) |
|:--:|
| **Segunda página com piadas** |

### Descrição das funcionalidades

A tela possui 3 botões, o objetivo é que eles disparem alguns eventos.

* **Buscar** deve disparar uma requisição HTTP/GET para uma API

* **Remover Ultimo** deve remover o ultimo elemento da lista

* **Voltar** deve voltar para a primeira tela

A tela também possui um elemento do tipo **SpinBox**

* **SpinBox** deve fornecer a quantidade de piadas a serem buscadas

Toda vez que uma piada é recebida ela deve ser apresentada na tela.

## API
A **API** utilizada está disponível no link https://www.icndb.com/ você deve ler e entender como ela funciona para poder utilizar no projeto.




# Critérios de avaliação

O projeto apresentado nesse desafio será avaliado nos seguintes quesitos:

* **Estrutura do projeto** - Como o projeto está dividido; Como o código está organizado; Como o código está comentado e documentado (60%)

* **Versionamento do projeto** - Quantidade e qualidade de commits - (20%)

* **Prazo** - 12:00 de 19 de Julho de 2021  (20%)