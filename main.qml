import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Window {
    property string ligthBlueColor: "#2e529f"
    property string darkBlueColor: "#051845"
    property string lightGrayColor: "#cacaca"

    width: 800
    height: 640
    visible: true
    title: qsTr("Skill Test")
    color: ligthBlueColor

    Rectangle {
        id: topRectId
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: darkBlueColor
        height: 50

        Label {
            anchors.centerIn: parent
            font.pixelSize: 30
            font.bold: true
            color: "white"
            text: "SkillTest"
        }
    }

    StackView {
        id: stackViewId
        anchors.top: topRectId.bottom
        anchors.bottom: bottomRectId.top
        anchors.right: parent.right
        anchors.left: parent.left

        initialItem: firstPageId

        Component {
            id: firstPageId

            FirstPage {}
        }

    }


    Rectangle {
        id: bottomRectId
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: darkBlueColor
        height: 50

        Image {
            anchors.centerIn: parent
            source: "images/logo.png"
            scale: 0.4
        }
    }
}
